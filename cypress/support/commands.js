// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************



//  
    Cypress.Commands.add("selectFontForText",(fontName =>{

        cy.iframe().find('div[class="blockbuilder-widget blockbuilder-font-family-widget"] div button[role="combobox"] span[class="label"]').realClick()
        cy.iframe().find('span').contains(fontName).realClick()


    }))



    Cypress.Commands.add("SelectFontColorForText",(fontColor =>{

      
        cy.iframe().find('div[id="color-picker-trigger"]').realClick()
        cy.iframe().find('input[id="rc-editable-input-1"]').clear().realType(fontColor)
     

    }))

  




//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })