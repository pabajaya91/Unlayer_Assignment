class EmailLayer
{

    getLoadedIframe()
    {
        return cy.frameLoaded('iframe[src="https://editor.unlayer.com/1.9.2/editor.html"]')
    }

    getTextBoxDrop()
    {
        return cy.iframe().find('div[class="blockbuilder-content-tools add-blank-space"] :nth-child(5)').first();
    }

    getNewlyAddedText()
    {
        return cy.iframe().find('p').contains("This is a new Text block. Change the text.")
    }

    getFontSelctor()
    {
        return cy.iframe().find('div[class="blockbuilder-widget blockbuilder-font-family-widget"] div button[role="combobox"] span[class="label"]')
    }

    getFontType()
    {
        return cy.iframe().find('span').contains("Cabin").realClick()
    }

    getColorPalletofText()
    {
        return cy.iframe().find('.sc-fhzFiK.bDWiQK.display-color.transparent')
    }

    getColor()
    {
        return cy.iframe().find('div[class="sc-kOHTFB ctZnJm"] div[class="circle"]')
    }

    getExportHTMLButton()
    {
        return cy.get('div[id="root"]').find('button').contains("Export HTML")
    }





}

export default EmailLayer;