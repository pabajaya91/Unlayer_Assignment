// <reference types ="cypress" />
/// <reference types ="cypress-iframe" />
import 'cypress-iframe'
import "cypress-real-events"
import chaiColors from 'chai-colors'
chai.use(chaiColors)


describe('Unlayer Test suite', function(){

    beforeEach(() => {

      cy.visit("https://react-email-editor-demo.netlify.app/")

    });

    it('Unlaye Drag and Drop Text Field', function()
    {

        //loadingIframe
        cy.frameLoaded('iframe[src="https://editor.unlayer.com/1.9.2/editor.html"]')

        
        const source = cy.iframe().find('div[class="blockbuilder-content-tools add-blank-space"] :nth-child(5)').first();
        cy.wait(2000)
        
       
        //Drag the text box and drop in the editor 
        source
          .realMouseDown()
        .realMouseMove(-500,0)
           .realMouseUp();
  
        cy.wait(2000)

        //Verifying Dragged item
        const textBoxText = cy.iframe().find('p').contains("This is a new Text block. Change the text.")
        textBoxText.realClick()
     
        cy.wait(2000)

        //Change the Font Family 
        var fontForText = "Cabin"
        cy.selectFontForText(fontForText)

        //Change Font Color
        var fontColorForText = "#0071E3"
        cy.SelectFontColorForText(fontColorForText)

        cy.wait(2000)

        //preparing to spy on the developer console
        cy.window()
        .its('console')
        .then((console) => {
        cy.spy(console, 'log').as('log')
        })


        // Click Export HTML 
        cy.get('div[id="root"]').find('button').contains("Export HTML").click().then(function(){

        //verifying the alert displyed after clicking export HTML
        cy.on('window:alert',(str)=>
        {
           expect (str).to.equal('Output HTML has been logged in your developer console')
        })
  
        })

        // Export the developer console content to a file
        cy.get('@log')
        .invoke('getCalls')
        .then((calls) => {
        const logText = calls[0].args[1]

        const filePath = './test-file.html';
        cy.writeFile(filePath, logText, 'utf-8');
        cy.visit(filePath);

        //Verifying Font Family in exported file
        cy.get('body').get('table [id="u_content_image_1"]+table div').should('have.css','font-family',"Cabin, sans-serif")
       
        //Verifying Font color in exported file
        cy.get('body').get('table [id="u_content_image_1"]+table div').should('have.css','color').and('be.colored', fontColorForText)

        });


    
  
    });
});



  
    






    
    
  